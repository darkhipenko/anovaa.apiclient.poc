﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anovaa.API.Contracts;
using Anovaa.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Anovaa.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private List<UserModel> _users;

        private readonly ILogger<UsersController> _logger;

        public UsersController(ILogger<UsersController> logger)
        {
            _logger = logger;
            _users = new List<UserModel>()
            {
                new UserModel() { Id = 1, Email = "user1@gmail.com", Name = "UserName1"},
                new UserModel() { Id = 2, Email = "user2@gmail.com", Name = "UserName2"},
                new UserModel() { Id = 3, Email = "user3@gmail.com", Name = "UserName3"},
            };
        }

        [HttpGet]
        [Route("")]
        public async Task<IReadOnlyList<UserDto>> GetAllUsers()
        {
            var result = _users.Select(x => x.MapToDto()).ToList();

            return await Task.FromResult(result);
        }

        [HttpGet]
        [Route("search")]
        public async Task<IReadOnlyList<UserDto>> SearchUsersBy([FromQuery]SearchUserByDto searchUserBy)
        {
            var query = _users.AsQueryable();
            if (searchUserBy.Id > 0)
                query = query.Where(x => x.Id == searchUserBy.Id);
            if (!string.IsNullOrEmpty(searchUserBy.Name))
                query = query.Where(x => x.Name.Equals(searchUserBy.Name, StringComparison.InvariantCultureIgnoreCase));
            if (!string.IsNullOrEmpty(searchUserBy.Email))
                query = query.Where(x => x.Email.Equals(searchUserBy.Email, StringComparison.InvariantCultureIgnoreCase));

            var users = query.ToList();
            var result = users.Select(x => x.MapToDto()).ToList();

            return await Task.FromResult(result);
        }

        [HttpPost]
        [Route("")]
        public Task AddUser([FromBody]UserDto newUserDto)
        {
            if (!_users.Exists(x => x.Id == newUserDto.Id))
            {
                var newUser = newUserDto.MapToModel();
                _users.Add(newUser);
            }

            return Task.CompletedTask;
        }

        [HttpPut]
        [Route("")]
        public Task UpdateUser([FromBody]UserDto updatedUser)
        {
            var oldUser = _users.FirstOrDefault(x => x.Id == updatedUser.Id);
            if (oldUser == null)
                return Task.CompletedTask;

            oldUser.Name = updatedUser.Name;
            oldUser.Email = updatedUser.Email;

            return Task.CompletedTask;
        }

        [HttpDelete]
        [Route("")]
        public Task DeleteUserById(int userId)
        {
            var oldUser = _users.FirstOrDefault(x => x.Id == userId);
            if (oldUser == null)
                return Task.CompletedTask;

            _users.Remove(oldUser);

            return Task.CompletedTask;
        }
    }
}