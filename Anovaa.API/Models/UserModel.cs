﻿using Anovaa.API.Contracts;

namespace Anovaa.API.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public static class UserExtensions
    {
        public static UserDto MapToDto(this UserModel userModel)
        {
            var result = new UserDto()
            {
                Id = userModel.Id,
                Name = userModel.Name,
                Email = userModel.Email
            };

            return result;
        }

        public static UserModel MapToModel(this UserDto userDto)
        {
            var result = new UserModel()
            {
                Id = userDto.Id,
                Name = userDto.Name,
                Email = userDto.Email
            };

            return result;
        }
    }
}