﻿namespace Anovaa.API.Contracts
{
    public class SearchUserByDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}