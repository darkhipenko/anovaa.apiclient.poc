﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RestEase;

namespace Anovaa.API.Contracts
{
    [AllowAnyStatusCode]
    // constant interface headers for all methods
    [Header("User-Agent", "RestEase")]
    [Header("Cache-Control", "no-cache")]
    public interface IUsersApi
    {
        // ----------------------------

        // header that applies to every single request, and whose value is variable, then use a variable interface header
        [Header("X-API-Key", "None")]
        string ApiKey { get; set; }

        // ----------------------------

        [Get("users")]
        Task<Response<IReadOnlyList<UserDto>>> GetAllUsers();

        [Get("users/search")]
        Task<Response<IReadOnlyList<UserDto>>> SearchUsersBy([Query] SearchUserByDto searchUserBy);

        [Post("users")]
        Task AddUser([Body]UserDto newUserDto);

        [Put("users")]
        Task UpdateUser([Body]UserDto updatedUser);

        [Delete("users/{userId}")]
        Task DeleteUser([Path] int userId);
    }
}