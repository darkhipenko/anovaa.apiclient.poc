﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Anovaa.API.Contracts
{
    public class UsersApiClient: IUsersApiClient
    {
        private readonly IUsersApi _usersApi;

        public UsersApiClient(IUsersApi usersApi)
        {
            _usersApi = usersApi;
        }

        public async Task<IReadOnlyList<UserDto>> GetAllUsers()
        {
            var response = await _usersApi.GetAllUsers();

            if (response.ResponseMessage.IsSuccessStatusCode) return response.GetContent();

            // check response.ResponseMessage.StatusCode
            // make custom logic
            // logging errors
            return new List<UserDto>();
        }

        public async Task<IReadOnlyList<UserDto>> SearchUsersBy(SearchUserByDto searchUserBy)
        {
            var response = await _usersApi.SearchUsersBy(searchUserBy);

            if (response.ResponseMessage.IsSuccessStatusCode) return response.GetContent();

            return new List<UserDto>();
        }

        public async Task AddUser(UserDto newUserDto)
        {
            await _usersApi.AddUser(newUserDto);
        }

        public async Task UpdateUser(UserDto updatedUser)
        {
            await _usersApi.UpdateUser(updatedUser);
        }

        public async Task DeleteUserById(int userId)
        {
            await _usersApi.DeleteUser(userId);
        }
    }
}