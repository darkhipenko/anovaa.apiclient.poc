﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Anovaa.API.Contracts
{
    public interface IUsersApiClient
    {
        Task<IReadOnlyList<UserDto>> GetAllUsers();
        Task<IReadOnlyList<UserDto>> SearchUsersBy(SearchUserByDto searchUserBy);
        Task AddUser(UserDto newUserDto);
        Task UpdateUser(UserDto updatedUser);
        Task DeleteUserById(int userId);
    }
}