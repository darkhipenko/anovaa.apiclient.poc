﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anovaa.API.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Anovaa.Consumer.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WebConsumer : ControllerBase
    {
        private readonly IUsersApiClient _apiClient;

        public WebConsumer(IUsersApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        [HttpGet]
        public async Task<IReadOnlyList<UserDto>> GetAllUsers()
        {
            return await _apiClient.GetAllUsers();
        }
    }
}