﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Anovaa.API.Contracts;
using Newtonsoft.Json;
using RestEase;

namespace Anovaa.Consumer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var api = RestClient.For<IUsersApi>("http://localhost:5000");
            var client = new UsersApiClient(api);

            var users = await client.GetAllUsers();
            var result = JsonConvert.SerializeObject(users);

            Console.WriteLine(result);
        }
    }
}